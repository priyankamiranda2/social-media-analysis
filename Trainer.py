from trainingoperations import TrainedData
#here we import trained data class so we can access method in it
class trainer(object):
#trainer has the object for optimizer and can access its methods
    def __init__(self, optimizer):
        super(trainer, self).__init__()
        self.o = optimizer
        #optimizer is object of optimizer
        self.data = TrainedData()
        #data is object of trained data 
    def train(self, text, className):
        #this method trains the data here 
        self.data.increaseClass(className)#done
        #increaseClass is a method of TrainedData that makes use of the type of data
        #here we classify the different tyoes of descriptions

        tokens = self.o.optimize(text)#done
        #tokenize method of optimizer uses the textual data sent to this method
        #tokens are returned
        #as in data that once was good morning son will now be returned as good,morning,son
        #all of the words are separated
        for token in tokens:
            #we dont really have stop words for now i guess
            token = self.o.remove_punctuation(token)#done
            #we remove punctuation and other useless things

            self.data.increaseToken(token, className)#done
            #we use object of trained data to access method increaseToken
            #every single token with its class is passed 
            #eg-hello,pos amazing,pos person,pos
