import re
#re means regular expression and it can carry out split(),sub()
class optimizer(object):
    def __init__(self, signs_to_remove = ["?!#%&"]):

        self.signs_to_remove = signs_to_remove
    def optimize(self, text):
        self.text=text
        return text.lower().split(' ')
#here, suppose we have the text- i am good.
#we get i,am,good.

    def remove_punctuation(self, token):
        token=token.replace(".","")
        token=token.replace(",","")
        token=token.replace("'","")
        token=token.replace("!","")
        token=token.replace("(","")
        token=token.replace(")","")
        token=token.replace("?","")
        token=token.replace("&","")

        return (token)