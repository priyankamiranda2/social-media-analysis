from __future__ import division
import operator
from functools import reduce
class Classifier(object):
    """docstring for Classifier"""
    def __init__(self, TrainedData, optimizer):
        super(Classifier, self).__init__()
        self.data = TrainedData
        # this can now access whatever data we have already trained and other methods there
        #the previously trained data includes simply the dictionary of each word with its class and its frequency
        self.optimizer = optimizer
        self.defaultProb = 0.000000001

    def classify(self, text):  
        documentCount = self.data.getDocCount()#checked
        text = self.optimizer.remove_punctuation(text)
        #we get a count of all the pos,neg and n values as in we get a total of all data previously existing
        classes = {'neg','pos'}
        #accesses the already trained data set and gets the evalues- pos,neg and n
        tokens = list(set(self.optimizer.optimize(text)))#checked
        #here we get all the separated words of the sentences with a comma
        probsOfClasses = {}

        for className in classes:          
            # we are calculating the probablity of seeing each token 
            # in the text of this class
            # P(Token_1|Class_i)

            tokensProbs = [self.getTokenProb(token, className) for token in tokens]
            #each word is checked for pos, neg and n probability
            # calculating the probablity of seeing the the set of tokens
            # in the text of this class
            # P(Token_1|Class_i) * P(Token_2|Class_i) * ... * P(Token_n|Class_i)
            #tokens probability has all the probabilities of every word in a caegory(pos,neg or n)
            try:
                tokenSetProb = reduce(lambda a,b: a*b, (i for i in tokensProbs if i) ) 
                        #this part multiplies all of them #this part returns all token probability values
            except:
                tokenSetProb = 0
            
            probsOfClasses[className] = tokenSetProb * self.getPrior(className)
            #P(Token_1|Class_i) * P(Token_2|Class_i) * ... * P(Token_n|Class_i)*(P(Class_i))/(P(Token_n)
        return sorted(probsOfClasses.items(), 
            key=operator.itemgetter(1), 
            reverse=True)
            #itemgetter is the key with with which we are supposed to sort the items. Since it is 1, it is based on the value of the probability not the className
            # we reverse it to get the highest probable value first

    def getPrior(self, className):
        # this method calculates probability of that class  in the data set- P(Class_i)
        return self.data.getClassDocCount(className) /  self.data.getDocCount()

    def getTokenProb(self, token, className):
        #p(token|Class_i)
        classDocumentCount = self.data.getClassDocCount(className)

        #here we check if that certain token(description)is there and if it is it sends the amount of data we have for that description
        # if the token is not seen in the training set, so not indexed,
        # then we return None not to include it into calculations.
        tokenFrequency = self.data.getFrequency(token, className)
        #we check the frequency of the repeating token in that class
        #we now have the number of times the word has srepeated in a certain category
        if classDocumentCount == 0 :
            return 0;
               
        else:
            probablity = (tokenFrequency / classDocumentCount)
        #number of times it repeats by total results is the probability of P(Token_1|Class_i)
            return probablity
                        