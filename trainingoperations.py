import sys

class TrainedData(object):
    def __init__(self):
        self.docCountOfClasses = {}
        self.frequencies = {}
    def increaseClass(self, className, byAmount = 1):

        self.docCountOfClasses[className] = self.docCountOfClasses.get(className, 0.0000001) + 1
        #in this method we count the different type of classes(decriptions) present(we only have neutral,pos,neg) their counts are stored beside them

    def increaseToken(self, token, className, byAmount = 1):
        if not token in self.frequencies:
                self.frequencies[token] = {}

        self.frequencies[token][className] = self.frequencies[token].get(className, 0.0000001) + 1
        #if the certain token with the classname isnt associated we store it, with its count if it is more than once

    def getDocCount(self):
        """
        returns all documents count
        """

        return sum(self.docCountOfClasses.values())

    def getClassDocCount(self, className):
        """
        returns document count of the class. 
        If class is not available, it returns None
        """

        return self.docCountOfClasses.get(className, 0)

    def getFrequency(self, token, className):
        if token in self.frequencies:
            #frequencies has tokens associated with the class
            #so if the token has been there before, we check the number of times it is associated with that class
            #we send that frequency 
            #or else we send 0
            foundToken = self.frequencies[token]#checked
            if foundToken.get(className)== None:
                return 0;
            else:    
                return foundToken.get(className)
        else:
            return 0;